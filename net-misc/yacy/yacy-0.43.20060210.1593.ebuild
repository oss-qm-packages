# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-misc/yacy/yacy-0.43.20060210.1593.ebuild,v 1.1 2006/02/10 13:03:22 slick Exp $

inherit eutils versionator

MAJOR_PV="$(get_version_component_range 1-2)"
REL_PV="$(get_version_component_range 3)"
SVN_PV="$(get_version_component_range 4)"

DESCRIPTION="YaCy - p2p based distributed web-search engine"
HOMEPAGE="http://www.yacy.net/"
SRC_URI="http://www.yacy.net/yacy/release/yacy_v${MAJOR_PV}_${REL_PV}_${SVN_PV}.tar.gz
http://download.berlios.de/yacy/yacy_v${MAJOR_PV}_${REL_PV}_${SVN_PV}.tar.gz"
SLOT="0"
KEYWORDS="~x86 ~amd64"
DEPEND="virtual/jdk
	app-admin/sudo"
LICENSE="GPL-2"

S="${WORKDIR}/yacy_v${MAJOR_PV}_${REL_PV}_${SVN_PV}"

src_install() {
	dodir /opt
	mv "${S}" "${D}/opt/yacy"
	chown -R yacy:yacy "${D}/opt/yacy"

	dodir /var/log/yacy
	chown yacy:yacy "${D}/var/log/yacy"
	dosed "s:DATA/LOG/:/var/log/yacy/:g" "/opt/yacy/yacy.logging"

	exeinto /etc/init.d
	newexe "${FILESDIR}/yacy-0.43.20060210.1593.rc" yacy
	insinto /etc/conf.d
	newins "${FILESDIR}/yacy-0.43.20060210.1593.confd" yacy
}

pkg_setup() {
	enewgroup yacy
	enewuser yacy -1 /bin/bash /opt/yacy yacy
}

pkg_postinst() {
	einfo "yacy.logging is prepared write logfiles to /var/log/yacy/"
	einfo "The initscript now supports new argumemts:"
	einfo "  migratewords | minimizeurldb | urldbcleanup | domlist | urllist"
	einfo "For details (in german) about this look at:"
	einfo "  http://yacy-websuche.de/wiki/index.php/De:GentooInstall"
}

