# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/mail-filter/procmail/procmail-3.22-r10.ebuild,v 1.10 2009/09/23 17:55:39 patrick Exp $

inherit eutils flag-o-matic toolchain-funcs

DESCRIPTION="Mail delivery agent/filter"
HOMEPAGE="http://www.procmail.org/"
SRC_URI="http://pubgit.metux.de/download/oss-qm/procmail/GENTOO.procmail-3.22.0.10.tar.bz2"

# Git repository: git://pubgit.metux.de/oss-qm/procmail (tag: GENTOO.procmail-${PV})

LICENSE="|| ( Artistic GPL-2 )"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 ~mips ppc ppc64 s390 sh sparc x86"
IUSE="mbox selinux"

DEPEND="virtual/mta"
RDEPEND="selinux? ( sec-policy/selinux-procmail )"
PROVIDE="virtual/mda"

src_unpack() {
	unpack ${A}
	cd ${WORKDIR}/*

	if ! use mbox ; then
		echo "# Use maildir-style mailbox in user's home directory" > ./procmailrc
		echo 'DEFAULT=$HOME/.maildir/' >> ./procmailrc
	else
		echo '# Use mbox-style mailbox in /var/spool/mail' > ./procmailrc
		echo 'DEFAULT=/var/spool/mail/$LOGNAME' >> ./procmailrc
	fi
}

src_compile() {
	cd ${WORKDIR}/*

	# -finline-functions (implied by -O3) leaves strstr() in an infinite loop.
	# To work around this, we append -fno-inline-functions to CFLAGS
	append-flags -fno-inline-functions
	append-flags -Werror

	if use mbox ; then
	    emake CC="$(tc-getCC)" CFLAGS0="${CFLAGS}" LDFLAGS0="${LDFLAGS}" || die
	else
	    emake CONF_MAILSPOOLDIR="" CC="$(tc-getCC)" CFLAGS0="${CFLAGS}" LDFLAGS0="${LDFLAGS}" || die
	fi
}

src_install() {
	cd ${WORKDIR}/*

	emake DESTDIR=${D} install

	fowners root:mail /usr/bin/lockfile
	fperms 2775 /usr/bin/lockfile
	fperms 6755 /usr/bin/procmail

	insopts -m 0644

	insinto /etc
	doins procmailrc || die
}

pkg_postinst() {
	if ! use mbox ; then
		elog "Starting with mail-filter/procmail-3.22-r9 you'll need to ensure"
		elog "that you configure a mail storage  location using DEFAULT in"
		elog "/etc/procmailrc, for example:"
		elog "\tDEFAULT=\$HOME/.maildir/"
	fi
}
