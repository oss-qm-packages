# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit eutils
DESCRIPTION="gfeed is a gtk+-2.0 based RSS/RDF feed reader. It currently supports most basic RSS/RDF feeds that do not include html."
HOMEPAGE="http://sourceforge.net/projects/gfeed/"
SRC_URI="mirror://sourceforge/gfeed/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

# Build-time dependencies, such as
#    ssl? ( >=dev-libs/openssl-0.9.6b )
#    >=dev-lang/perl-5.6.1-r1
# It is advisable to use the >= syntax show above, to reflect what you
# had installed on your system when you tested the package.  Then
# other users hopefully won't be caught without the right version of
# a dependency.
DEPEND=">=x11-libs/gtk+-2.0
	>=dev-libs/glib-2.0
	>=dev-libs/libxml2-2.5.0
	>=net-libs/gnet-2.0.7"
RDEPEND="${DEPEND}"

src_compile() {
	econf || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake -j1 DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS INSTALL NEWS README ChangeLog ecvs.feed gfeed.feed sfbugtraq.feed sfnews.feed slash.feed slashtest.feed
	docinto html
	dodoc doc/out/html/agg.html doc/out/html/overview.html
#	dodoc doc/out/html/agg.html doc/out/html/overview.html
}

pkg_postinst() {
	einfo "Please configure your news feeds, or see the examples in the"
	einfo "documentation directory"
}
