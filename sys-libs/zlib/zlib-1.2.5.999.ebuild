# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-libs/zlib/zlib-1.2.5.ebuild,v 1.2 2010/04/20 20:34:54 vapier Exp $

EGIT_REPO_URI="git://pubgit.metux.de/oss-qm/${PN}"
EGIT_BRANCH="METUX.${PN}.master"
EGIT_COMMIT="refs/tags/METUX.${PN}-${PV}"

inherit eutils toolchain-funcs git

DESCRIPTION="Standard (de)compression library"
HOMEPAGE="http://www.zlib.net/"
SRC_URI=""

LICENSE="ZLIB"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~sparc-fbsd ~x86-fbsd"
IUSE=""

RDEPEND="!<dev-libs/libxml2-2.7.7" #309623

src_unpack() {
	git_src_unpack
}

src_compile() {
	case ${CHOST} in
	*-mingw*|mingw*)
		cp zconf.h.in zconf.h
		emake -f win32/Makefile.gcc prefix=/usr STRIP=true PREFIX=${CHOST}- || die
		;;
	*)
		econf --shared || die
		emake || die
		;;
	esac
}

src_install() {
	case ${CHOST} in
	*-mingw*|mingw*)
		emake -f win32/Makefile.gcc prefix=/usr install DESTDIR="${D}" || die
		dodoc FAQ README ChangeLog doc/*.txt
		dobin zlib1.dll || die
		dolib libz.dll.a || die
		;;
	*)
		emake install DESTDIR="${D}" || die
		dodoc FAQ README ChangeLog doc/*.txt
		gen_usr_ldscript -a z
		;;
	esac
}
