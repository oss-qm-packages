# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /home/cvs/repositories/oss-qm/oss-qm-overlay/sys-libs/libgpm/libgpm-1.20.1-r5.ebuild,v 1.1.1.1 2007/05/17 17:11:49 nekrad Exp $

# emacs support disabled due to Bug 99533

inherit eutils toolchain-funcs

DESCRIPTION="Console-based mouse driver"
HOMEPAGE="http://www.metux.de/released/libgpm/"
SRC_URI="http://www.metux.de/released/libgpm/libgpm-1.20.1.0.tar.bz2"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 m68k mips ppc ppc64 s390 sh sparc x86"
IUSE="selinux"

DEPEND="sys-libs/ncurses"

src_unpack() {
	unpack ${A}
	echo "aa"
	cd "${S}"
	echo "bb"
}

src_compile() {
	autoreconf -fi
	econf \
		--libdir=/$(get_libdir) \
		--sysconfdir=/etc/gpm \
		|| die "econf failed"
	emake \
		CC=$(tc-getCC) \
		AR=$(tc-getAR) \
		RANLIB=$(tc-getRANLIB) \
		|| die "emake failed"
}

## we install shared libs to /lib but static ones to /usr/lib
src_install() {
	make install DESTDIR="${D}" || die "make install failed"
	# fix lib symlinks since the default is missing/bogus
#	dosym libgpm.so.1.19.0 /$(get_libdir)/libgpm.so.1
#	dosym libgpm.so.1 /$(get_libdir)/libgpm.so
	dodir /usr/$(get_libdir)
	mv "${D}"/$(get_libdir)/*.a "${D}"/usr/$(get_libdir)/
	mkdir -p "${D}"/usr/$(get_libdir)/pkgconfig
	mv "${D}"/$(get_libdir)/pkgconfig/*.pc "${D}"/usr/$(get_libdir)/pkgconfig
	rmdir "${D}"/$(get_libdir)/pkgconfig/
	gen_usr_ldscript libgpm.so
}
