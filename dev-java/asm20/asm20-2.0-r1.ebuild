# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /home/cvs/repositories/oss-qm/oss-qm-overlay/dev-java/asm20/asm20-2.0-r1.ebuild,v 1.1 2008/03/14 22:55:35 nekrad Exp $

inherit java-pkg-2 java-ant-2

DESCRIPTION="Bytecode manipulation framework for Java"
HOMEPAGE="http://asm.objectweb.org"
SRC_URI="http://download.forge.objectweb.org/asm/asm-${PV}.tar.gz"
LICENSE="BSD"
KEYWORDS="amd64 ~ia64 ppc ~ppc64 x86"
SLOT="0"
IUSE="doc source"
DEPEND=">=virtual/jdk-1.3
	dev-java/ant-core
	dev-java/ant-owanttask
	source? ( app-arch/zip )"
RDEPEND=">=virtual/jre-1.3"
RESTRICT="test"

src_unpack() {
	ln -s asm-${PV}.tar.gz asm20-${PV}.tar.gz
	unpack ${A}
	ln -s asm-${PV} asm20-${PV}
	cd ${S}
	epatch ${FILESDIR}/asm20-namespace.patch
	echo "objectweb.ant.tasks.path /usr/share/ant-owanttask/lib/ow_util_ant_tasks.jar" \
		>> build.properties
}

src_compile() {
	eant jar $(use_doc jdoc)
}

src_install() {
	for x in output/dist/lib/*.jar ; do
		java-pkg_newjar ${x} $(basename ${x/-${PV}})
	done
	use doc && java-pkg_dohtml -r output/dist/doc/javadoc/user/*
	use source && java-pkg_dosrc src/*
}
